const { getStructure, runSql, createTableWithNewProp, createCopyDb, commitCopyDb } = require('./utils/sqliteServer.js')

const getErrorText = (e) => {
  let error = e
  if (typeof e === 'object' && e.message) error = e.message
  console.log('Error', e)
  return error
}

module.exports = function (RED) {
  function sqliteTab (config) {
    RED.nodes.createNode(this, config)
  }
  RED.nodes.registerType('sqlite-plugin-red', sqliteTab)

  RED.httpAdmin.get('/getSqliteDbStructure', (request, response) => {
    try {
      const database = getStructure(request.query.path)
      return response.send(database)
    } catch (e) {
      response.statusCode = 500
      return response.send(getErrorText(e))
    }
  })

  RED.httpAdmin.get('/handleSqliteRequest', (request, response) => {
    const q = request.query
    try {
      let result
      // for SQL run / get commands
      if (q.command) {
        result = runSql(q.path, q.sql, q.command)
      // for actions like copy db with more than one command
      } else if (q.action) {
        if (q.action === 'createCopy') {
          result = createCopyDb(q.path, q.fields, q.fieldsql, q.tablename, q.backupname)
        } else if (q.action === 'commit') {
          result = commitCopyDb(q.path, q.tablename, q.backupname)
        } else if (q.action === 'createTableNewProp') {
          result = result = createTableWithNewProp(q.path, q.fields, q.fieldsql, q.backupname)
        } else {
          throw new Error('Unknown action')  
        }
      } else {
        throw new Error('Missing command or action')
      }
      return response.send(result)
    } catch (e) {
      response.statusCode = 500
      return response.send(getErrorText(e))
    }
  })
}

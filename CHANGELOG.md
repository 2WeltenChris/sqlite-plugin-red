# Changelog

## Version 0.0.3 (25th of August 2021)

- updated style to latest SIR
- delete row bugfix
- renamed to sqlite-plugin-red

## Version 0.0.2 (17th of August 2021)

- updated style to latest SIR
- added cancel edit table function

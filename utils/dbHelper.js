const clearDbStructure = (selected) => {
  selected.database = []
  selected.path = ''
  selected.table = null
  return selected
}

const getStructure = (selected) => {
  return new Promise((resolve, reject) => {
    if (selected.databaseId) {
      if (!selected.path) {
        reject(new Error('Database not found!'))
      }
      window.$.get('getSqliteDbStructure?path=' + selected.path)
        .success((structure) => {
          selected.database = structure
          resolve(selected)
        }).fail((e) => {
          clearDbStructure(selected)
          reject(e)
        })
    } else {
      clearDbStructure(selected)
      resolve(selected)
    }
  })
}

module.exports = {
  clearDbStructure,
  getStructure
}

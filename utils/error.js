const setError = (e, doThrow) => {
  let errorMsg = e
  if (e.message) errorMsg = e.message
  else if (e.responseText) errorMsg = e.responseText
  console.log('Error', errorMsg, e)
  RED.notify(errorMsg, { type: 'error', modal: true, timeout: 60000 })
  if (doThrow) throw new Error(errorMsg)
}

module.exports = {
  setError
}
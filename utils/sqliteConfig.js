/* global RED */
const edit = (sqliteConfigNodes, selected) => {
  const db = sqliteConfigNodes.find(database => database.id === selected.databaseId)
  RED.editor.editConfig('', db.type, db.id)
  getNodes(sqliteConfigNodes)
  return sqliteConfigNodes
}

const add = (sqliteConfigNodes) => {
  try {
    RED.editor.editConfig('', 'sqlitedb', '_ADD_')
  } catch (e) {
    throw new Error('node-red-node-sqlite is not installed!')
  }
  getNodes(sqliteConfigNodes)
  return sqliteConfigNodes
}

const getNodes = (sqliteConfigNodes) => {
  sqliteConfigNodes = []
  RED.nodes.eachConfig(function (cn) {
    // TODO if working. check memory db
    if (cn.type === 'sqlitedb' && cn.db !== ':memory:') {
      sqliteConfigNodes.push(cn)
    }
  })
  return sqliteConfigNodes
}

module.exports = {
  edit,
  add,
  getNodes
}

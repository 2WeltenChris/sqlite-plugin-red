# Sqlite Tab RED

Adding a sidebar tab for Node-RED to show and edit sqlite database structures

## Requirements

Requires the Node-RED node: [node-red-node-sqlite](https://flows.nodered.org/node/node-red-node-sqlite).

## Instructions

The UI will help you to visualize and create your sqlite database, all within Node-RED.

- Select, create or delete your your sqlite database files in the first row.
- Select, create, rename or delete your tables in the second row.
- If a table is selected you can add, remove or change your sqlite table columns.
- If done press the confirm button.

![Screenshot of a the Sqlite Tab.](https://gitlab.com/2WeltenChris/sqlite-plugin-red/-/raw/main/examples/sqlite.png)

## Notes

There is currently no input safeproof implemented, as sqlite will return an error if something is not correct.
For example: Creating a autoincrement column without setting the column to INTEGER.

This will be planned in the future.

## Developers

If you want to modify something inside the sqlite-plugin-red.html file, I recommend to use [SIR](https://gitlab.com/2WeltenChris/svelte-integration-red).

With help of SIR you can handle the sqlite-plugin-red.svelte file in which the code is much cleaner and easier to handle.
